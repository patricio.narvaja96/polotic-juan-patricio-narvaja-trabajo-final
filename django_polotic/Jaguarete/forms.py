from django import forms
from django.db.models.enums import Choices
from django.forms.widgets import Select
from .models import Producto,Categoria


class RegistroProducto(forms.Form):
    Titulo = forms.CharField(label='Titulo', widget=forms.TextInput(), max_length=30, required=True, help_text='Requerido. 30 caracteres o menos. Letras, dígitos y @ /. / + / - / _ solamente.')
    Imagen = forms.ImageField(label='Imagen', widget=forms.FileInput(), required=True, help_text='Requerido. Imagen formato JPG/JPEG.')
    Descripcion = forms.CharField(label='Descripcion', widget=forms.TextInput(), required=True, help_text='Requerido. 120 caracteres o menos. Letras, dígitos y @ /. / + / - / _ solamente.')
    Precio = forms.IntegerField(label='Precio', widget=forms.NumberInput(), required=True, help_text='Requerido.')
    Categoria = forms.ChoiceField(label='Categoria', widget=forms.Select(choices=Categoria.objects.all()), required=True, help_text='Requerido. Seleccione una opcion')
    
    class Meta:
        model = Producto
        fields = ('Titulo','Imagen', 'Descripcion', 'Precio', 'Categoria' )