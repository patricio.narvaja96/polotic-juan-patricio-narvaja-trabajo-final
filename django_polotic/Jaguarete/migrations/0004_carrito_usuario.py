# Generated by Django 3.2.4 on 2021-07-05 22:20

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('Jaguarete', '0003_alter_producto_imagen'),
    ]

    operations = [
        migrations.AddField(
            model_name='carrito',
            name='Usuario',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuario', to=settings.AUTH_USER_MODEL),
        ),
    ]
