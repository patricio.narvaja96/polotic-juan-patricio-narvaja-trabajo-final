from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Categoria(models.Model):
    Descripcion = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.Descripcion}"

class Producto(models.Model):
    Titulo = models.CharField(max_length=30)
    Imagen = models.ImageField()
    Descripcion = models.CharField(max_length=120)
    Precio = models.IntegerField()
    Categoria = models.ForeignKey(Categoria,on_delete=models.CASCADE,related_name="Productos")

    def __str__(self):
        return f"Producto {self.id}:({self.Categoria}) {self.Titulo} - {self.Descripcion} --- ${self.Precio}"

class Carrito(models.Model):
    Usuario = models.ForeignKey(User,on_delete=models.CASCADE,related_name="Usuario",null=True) 
    ListaProductos = models.ManyToManyField(Producto,blank=True,related_name="Compras")
    TotalCarrito = models.IntegerField(null=True)

    def __str__(self):
        return f"Carrito de {self.Usuario} ${self.TotalCarrito}"

