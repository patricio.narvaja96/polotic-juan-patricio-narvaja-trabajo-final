from django.urls import path
from . import views

app_name = "Jaguarete"
urlpatterns = [
    path('',views.index, name="inicio"),
    path('about',views.about, name="acercade"),
    path('search',views.search, name="busqueda"),
    path('Categoria',views.BusquedaCategoria, name="BusquedaCategoria"),
    path('Categoria/<int:id_categoria>',views.BusquedaCategoria, name="BusquedaCategoria"),
    path('Producto',views.producto, name="Producto"),
    path('Producto/<int:id_producto>',views.producto, name="Producto"),
    path('Producto/ProductoNuevo',views.ProductoNuevo, name="ProductoNuevo"),
    path('Producto/editarproducto',views.productoeditar, name="ProductoEditar"),
    path('VerCarrito',views.verCarrito, name="Carrito"),
    
]
