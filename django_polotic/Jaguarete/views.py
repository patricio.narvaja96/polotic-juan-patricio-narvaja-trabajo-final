from django.contrib.auth.models import User
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse
from .models import Categoria, Producto
from django.contrib.auth.decorators import login_required
from .forms import *



# Create your views here.
def index(request):
    if "leer_mas_tarde" not in request.session:
        request.session["Carrito"] = []
    categorias = Categoria.objects.all()
    allprods = Producto.objects.all().order_by('-id')
    prodsPrincipales = allprods[:3]
    prodsFinales = allprods[3:10]
    return render(request,"jaguarete/main.html",{"lista_Categorias": categorias,"Carrito":request.session["Carrito"] ,"prodsPrincipales":prodsPrincipales,"prodsFinales":prodsFinales})

def BusquedaCategoria(request,id_categoria):
    categorias = Categoria.objects.all()
    categ = get_object_or_404(Categoria,id=id_categoria)
    prod = Producto.objects.all().filter(Categoria=categ)
    return render(request, "jaguarete/search.html",{"lista_Categorias": categorias,"productos":prod, "Palabra":categ})

def search(request,id_categoria):
    categorias = Categoria.objects.all()
    categ = get_object_or_404(Categoria,id=id_categoria)
    prods = Producto.objects.all().filter(Categoria=categ)
    return render(request, "jaguarete/search.html",{"lista_Categorias": categorias,"productos": prods,"Palabra":categ.Descripcion})

def producto(request,id_producto):
    categorias = Categoria.objects.all()
    prod = get_object_or_404(Producto, id=id_producto)
    return render(request, "jaguarete/producto.html",{"lista_Categorias": categorias , "producto":prod})

def about(request):
    categorias = Categoria.objects.all()
    return render(request, "jaguarete/about.html",{"lista_Categorias": categorias})


@login_required
def ProductoNuevo(request):
    categorias = Categoria.objects.all()
    if request.method == 'POST':
        form = RegistroProducto(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('login'))
    else:
        form = RegistroProducto()
    return render(request, 'jaguarete/productonuevo.html', {'form': form,"lista_Categorias":categorias})

@login_required
def verCarrito(request):
    if request.user.is_superuser:
        return HttpResponseRedirect('/')
    else:
        categorias = Categoria.objects.all()
        return render(request, "jaguarete/carrito.html",{"lista_Categorias": categorias})

@login_required
def productoeditar(request):
    if request.user.is_superuser:
        categorias = Categoria.objects.all()
        return render(request, "jaguarete/productoeditar.html",{"lista_Categorias": categorias})
    else:
        return HttpResponseRedirect('/')

# @login_required
# def leer_mas_tarde(request, articulo_id):
#     un_articulo = get_object_or_404(Articulo, id=articulo_id)
#     for id in request.session["leer_mas_tarde"]:
#         if id == articulo_id:
#             #existe el articulo
#             return HttpResponseRedirect(reverse("sitio:articulo", args=(un_articulo.id,)))            
#     request.session["leer_mas_tarde"] += [articulo_id]
#     return HttpResponseRedirect(reverse("sitio:articulo", args=(un_articulo.id,)))   
